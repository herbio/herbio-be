FROM openjdk:13-alpine
ADD target/*.jar app.jar
EXPOSE 8095
ENTRYPOINT ["java","-jar","/app.jar"]