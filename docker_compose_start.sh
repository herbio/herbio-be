#!/bin/sh

# remove all running containers
docker rm -f `docker ps -aq`

# remove all existing volumes
docker volume rm `docker volume ls -q`

# give permissions to 'docker' directory
echo sixonenine619 | sudo -S chmod 777 -R docker

# pull images from registry
docker-compose pull

# run 'docker-compose.yml' file, which starts all containers
docker-compose up