#!/bin/sh

# shut down docker compose
docker-compose down

# remove all running containers
docker rm -f `docker ps -aq`

# remove all existing volumes
docker volume rm `docker volume ls -q`