package sk.stuba.fei.api.herbiobe;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.password.PasswordEncoder;
import sk.stuba.fei.api.herbiobe.config.SwaggerConfiguration;
import sk.stuba.fei.api.herbiobe.model.entity.*;
import sk.stuba.fei.api.herbiobe.repository.RoleRepository;
import sk.stuba.fei.api.herbiobe.repository.RoomRepository;
import sk.stuba.fei.api.herbiobe.repository.UserRepository;

import java.util.List;
import java.util.Set;

@Slf4j
@EnableAsync
@SpringBootApplication
@Import(SwaggerConfiguration.class)
public class HerbioBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(HerbioBeApplication.class, args);
    }

    @Bean
    @ConditionalOnProperty(prefix = "spring.jpa.hibernate", name = "ddl-auto", havingValue = "update")
    public CommandLineRunner init(
        UserRepository userRepository,
        RoleRepository roleRepository,
        PasswordEncoder passwordEncoder,
        RoomRepository roomRepository
    ) {
        return args -> {
            log.info("Creating schemas");
            saveDummyRolesAndUsers(roleRepository, userRepository, passwordEncoder);
            saveDummyRoom(roomRepository);
        };
    }

    private void saveDummyRoom(final RoomRepository roomRepository) {
        var waterTank = WaterTankEntity.dummyWaterTank();
        var wateringProfile = WateringProfileEntity.dummyProfile();
        var plant = PlantEntity.dummyPlant().addWateringProfile(wateringProfile);
        var room = RoomEntity.dummyRoom().addWaterTank(waterTank).addPlant(plant);
        roomRepository.save(room);
    }

    private void saveDummyRolesAndUsers(
        final RoleRepository roleRepository,
        final UserRepository userRepository,
        final PasswordEncoder passwordEncoder
    ) {
        var roleUser = roleRepository.save(
            RoleEntity
                .builder()
                .name(ERole.ROLE_USER)
                .build()
        );

        var roleAdmin = roleRepository.save(
            RoleEntity
                .builder()
                .name(ERole.ROLE_ADMIN)
                .build()
        );

        userRepository.saveAll(List.of(
            UserEntity
                .dummyUser()
                .withPassword(passwordEncoder.encode("password"))
                .withRoles(Set.of(roleUser)),
            UserEntity
                .dummyAdmin()
                .withPassword(passwordEncoder.encode("password"))
                .withRoles(Set.of(roleUser, roleAdmin))
        ));
    }
}
