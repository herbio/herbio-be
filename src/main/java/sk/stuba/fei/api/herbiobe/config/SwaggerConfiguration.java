package sk.stuba.fei.api.herbiobe.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket swaggerAuth() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("AUTH")
            .select()
            .paths(PathSelectors.ant("/auth/**"))
            .apis(RequestHandlerSelectors.any())
            .build()
            .apiInfo(getAuthInfo());
    }

    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("API")
            .select()
            .paths(PathSelectors.ant("/api/**"))
            .apis(RequestHandlerSelectors.any())
            .build()
            .apiInfo(getApiInfo());
    }

    private ApiInfo getAuthInfo() {
        return new ApiInfoBuilder()
            .title("HERBIO-AUTH")
            .version("1.0")
            .description("REST API documentation for HerbIO app authorization")
            .contact(getCorporateContact())
            .build();
    }

    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
            .title("HERBIO-API")
            .version("1.0")
            .description("REST API documentation for HerbIO application")
            .contact(getCorporateContact())
            .build();
    }

    private Contact getCorporateContact() {
        return new Contact(
            "Slovak Technical University",
            "https://www.stuba.sk/",
            "public@stuba.sk"
        );
    }
}
