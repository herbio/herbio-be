package sk.stuba.fei.api.herbiobe.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sk.stuba.fei.api.herbiobe.model.payload.request.LoginRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.RefreshTokenRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.SignupRequest;
import sk.stuba.fei.api.herbiobe.model.payload.response.AuthenticationResponse;
import sk.stuba.fei.api.herbiobe.model.payload.response.MessageResponse;
import sk.stuba.fei.api.herbiobe.service.auth.AuthService;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping(value = "auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @ApiOperation(value = "Log in to the application")
    @PostMapping(value = "login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthenticationResponse> login(@Valid @RequestBody LoginRequest loginRequest) {
        return authService.login(loginRequest);
    }

    @ApiOperation(value = "Log out from the application. JWT will be removed after this operation")
    @PostMapping(value = "logout", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageResponse> logout(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        return authService.logout(refreshTokenRequest);
    }

    @ApiOperation(value = "Refresh the user and get a new JWT with the refresh token provided when logging in")
    @PostMapping(value = "refresh", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthenticationResponse> refreshToken(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        return authService.refreshToken(refreshTokenRequest);
    }

    @ApiOperation("Sign up to the application")
    @PostMapping(value = "signup", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageResponse> signup(@Valid @RequestBody SignupRequest signupRequest) {
        return authService.signUp(signupRequest);
    }
}
