package sk.stuba.fei.api.herbiobe.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.stuba.fei.api.herbiobe.model.payload.response.MessageResponse;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@CrossOrigin
@RestController
@RequestMapping("auth/test")
public class AuthTestController {

    @GetMapping("all")
    public ResponseEntity<MessageResponse> allAccess() {
        return ResponseEntity.ok(new MessageResponse("Public access"));
    }

    @GetMapping("user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<MessageResponse> userAccess() {
        return ResponseEntity.ok(new MessageResponse("User access"));
    }

    @GetMapping("admin")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<MessageResponse> adminAccess() {
        return ResponseEntity.ok(new MessageResponse("Admin access"));
    }
}
