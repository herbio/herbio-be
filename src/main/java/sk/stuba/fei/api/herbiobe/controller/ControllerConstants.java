package sk.stuba.fei.api.herbiobe.controller;

public class ControllerConstants {

    private ControllerConstants() {
    }

    public static final String CANNOT_FIND_ROOM_WITH_ID = "Cannot find room with id ";
    public static final String CANNOT_FIND_PLANT_WITH_ID = "Cannot find plant with id ";
    public static final String CANNOT_FIND_WATER_TANK_WITH_ID = "Cannot find water tank with id ";
    public static final String CANNOT_FIND_WATERING_PROFILE_WITH_ID = "Cannot find watering profile with id ";
    public static final String PLANT_ALREADY_HAS_WATERING_PROFILE = "Plant already has watering profile";

    // info messages
    public static final String INF_SSE_CONSUMER_CONNECTED = "SSE consumer connected";
    public static final String INF_SENT_WATERING_MODE_NOTIF = "Sent notification about watering mode change for plant with id: '{}'. New mode: '{}'";

    // error messages
    public static final String ERR_SSE_EMITTER_BROKEN_PIPE = "SSE emitter failed, removing it";
    public static final String ERR_CANNOT_NOTIFY_DUE_JSON_PARSE_ERROR = "Couldn't send notification to topic '{}' due to JSON parse error";

    // warning messages
    public static final String INF_SSE_EMITTER_COUNT = "SSE emitter count: {}";
}
