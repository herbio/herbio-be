package sk.stuba.fei.api.herbiobe.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping("hello")
public class HelloWorldController {

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String hello() {
        return "Hello World!";
    }
}
