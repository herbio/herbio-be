package sk.stuba.fei.api.herbiobe.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import sk.stuba.fei.api.herbiobe.model.measurement.PlantMeasurement;

import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@Controller
@CrossOrigin
@RequestMapping("live")
public class LiveController {

    private final CopyOnWriteArrayList<SseEmitter> emitters = new CopyOnWriteArrayList<>();

    @GetMapping(path = "plant", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter handle(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-store");

        var emitter = new SseEmitter();
        emitters.add(emitter);

        emitter.onCompletion(() -> this.emitters.remove(emitter));
        emitter.onTimeout(() -> this.emitters.remove(emitter));

        log.info(ControllerConstants.INF_SSE_CONSUMER_CONNECTED);
        log.info(ControllerConstants.INF_SSE_EMITTER_COUNT, emitters.size());

        return emitter;
    }

    @EventListener
    public void onPublishPlantInfo(final PlantMeasurement plantMeasurement) {
        emitters.forEach(emitter -> {
            try {
                emitter.send(plantMeasurement);
            } catch (Exception e) {
                log.error(ControllerConstants.ERR_SSE_EMITTER_BROKEN_PIPE);
                emitters.remove(emitter);
                log.info(ControllerConstants.INF_SSE_EMITTER_COUNT, emitters.size());
            }
        });
    }
}
