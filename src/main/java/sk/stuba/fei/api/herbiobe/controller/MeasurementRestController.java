package sk.stuba.fei.api.herbiobe.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.stuba.fei.api.herbiobe.model.measurement.PlantMeasurement;
import sk.stuba.fei.api.herbiobe.model.measurement.RoomMeasurement;
import sk.stuba.fei.api.herbiobe.model.measurement.WaterTankMeasurement;
import sk.stuba.fei.api.herbiobe.service.influxdb.InfluxDBHandler;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/measurements")
@RequiredArgsConstructor
public class MeasurementRestController {

    private final InfluxDBHandler influxDBHandler;

    @GetMapping("plant")
    public List<PlantMeasurement> getPlantMeasurements() {
        return influxDBHandler.getPlantMeasurements();
    }

    @GetMapping("room")
    public List<RoomMeasurement> getRoomMeasurements() {
        return influxDBHandler.getRoomMeasurements();
    }

    @GetMapping("watertank")
    public List<WaterTankMeasurement> getWaterTankMeasurements() {
        return influxDBHandler.getWaterTankMeasurements();
    }
}
