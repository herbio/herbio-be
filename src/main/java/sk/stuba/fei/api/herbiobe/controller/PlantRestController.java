package sk.stuba.fei.api.herbiobe.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sk.stuba.fei.api.herbiobe.exception.ResourceNotFoundException;
import sk.stuba.fei.api.herbiobe.model.entity.PlantEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.CreatePlantRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.UpdatePlantRequest;
import sk.stuba.fei.api.herbiobe.repository.PlantRepository;
import sk.stuba.fei.api.herbiobe.repository.RoomRepository;
import sk.stuba.fei.api.herbiobe.service.mapper.PlantMapper;

import java.util.List;

import static sk.stuba.fei.api.herbiobe.controller.ControllerConstants.CANNOT_FIND_PLANT_WITH_ID;
import static sk.stuba.fei.api.herbiobe.controller.ControllerConstants.CANNOT_FIND_ROOM_WITH_ID;

@CrossOrigin
@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class PlantRestController {

    private final PlantRepository plantRepository;
    private final RoomRepository roomRepository;
    private final PlantMapper plantMapper;

    @ApiOperation("Get all plants in the specified room")
    @GetMapping(value = "rooms/{roomId}/plants", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PlantEntity> findAllByRoom(@PathVariable Integer roomId) {
        if (!roomRepository.existsById(roomId)) {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId);
        } else {
            return plantRepository.findByRoomId(roomId);
        }
    }

    @ApiOperation("Get plant by room and id")
    @GetMapping(value = "rooms/{roomId}/plants/{plantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PlantEntity findByRoom(@PathVariable Integer roomId, @PathVariable Integer plantId) {
        if (!roomRepository.existsById(roomId)) {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId);
        } else {
            return plantRepository.findByIdAndRoomId(plantId, roomId)
                .orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_PLANT_WITH_ID + plantId));
        }
    }

    @ApiOperation("Create plant in specified room")
    @PostMapping(
        value = "rooms/{roomId}/plants",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public PlantEntity create(@PathVariable Integer roomId, @RequestBody CreatePlantRequest createPlantRequest) {
        return roomRepository.findById(roomId).map(room -> {
            var plant = plantMapper.createEntityFromRequest(createPlantRequest);
            room.addPlant(plant);
            return plantRepository.save(plant);
        }).orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId));
    }

    @ApiOperation("Update plant in specified room")
    @PutMapping(
        value = "rooms/{roomId}/plants/{plantId}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public PlantEntity update(
        @PathVariable Integer roomId,
        @PathVariable Integer plantId,
        @RequestBody UpdatePlantRequest updatePlantRequest
    ) {
        if (!roomRepository.existsById(roomId)) {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId);
        }

        return plantRepository.findById(plantId).map(plant -> {
            plantMapper.mapRequestToEntity(updatePlantRequest, plant);
            return plantRepository.save(plant);
        }).orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_PLANT_WITH_ID + plantId));
    }

    @ApiOperation("Delete PlantEntity from specified room")
    @DeleteMapping("rooms/{roomId}/plants/{plantId}")
    public void delete(@PathVariable Integer roomId, @PathVariable Integer plantId) {
        if (!roomRepository.existsById(roomId)) {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId);
        } else if (plantRepository.existsById(plantId)) {
            plantRepository.deleteById(plantId);
        } else {
            throw new ResourceNotFoundException(CANNOT_FIND_PLANT_WITH_ID + plantId);
        }
    }
}
