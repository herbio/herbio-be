package sk.stuba.fei.api.herbiobe.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sk.stuba.fei.api.herbiobe.exception.ResourceNotFoundException;
import sk.stuba.fei.api.herbiobe.model.entity.RoomEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.CreateRoomRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.UpdateRoomRequest;
import sk.stuba.fei.api.herbiobe.repository.RoomRepository;
import sk.stuba.fei.api.herbiobe.service.mapper.RoomMapper;

import java.util.List;
import java.util.Optional;

import static sk.stuba.fei.api.herbiobe.controller.ControllerConstants.CANNOT_FIND_ROOM_WITH_ID;

@CrossOrigin
@RestController
@RequestMapping("api/rooms")
@RequiredArgsConstructor
public class RoomRestController {

    private final RoomRepository roomRepository;
    private final RoomMapper roomMapper;

    @ApiOperation("Get all rooms")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RoomEntity> findAll() {
        return roomRepository.findAll();
    }

    @ApiOperation("Get room by id")
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<RoomEntity> findById(@PathVariable Integer id) {
        return roomRepository.findById(id);
    }

    @ApiOperation("Create new room")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RoomEntity create(@RequestBody CreateRoomRequest createRoomRequest) {
        return roomRepository.save(roomMapper.createEntityFromRequest(createRoomRequest));
    }

    @ApiOperation("Update room by id")
    @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RoomEntity update(@PathVariable Integer id, @RequestBody UpdateRoomRequest updateRoomRequest) {
        var roomEntity = roomRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + id));
        roomMapper.mapRequestToEntity(updateRoomRequest, roomEntity);
        return roomRepository.save(roomEntity);
    }

    @ApiOperation("Delete room by id")
    @DeleteMapping("{id}")
    public void deleteById(@PathVariable Integer id) {
        if (roomRepository.existsById(id)) {
            roomRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + id);
        }
    }
}
