package sk.stuba.fei.api.herbiobe.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sk.stuba.fei.api.herbiobe.exception.ResourceNotFoundException;
import sk.stuba.fei.api.herbiobe.model.entity.WaterTankEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.CreateWaterTankRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.UpdateWaterTankRequest;
import sk.stuba.fei.api.herbiobe.repository.RoomRepository;
import sk.stuba.fei.api.herbiobe.repository.WaterTankRepository;
import sk.stuba.fei.api.herbiobe.service.mapper.WaterTankMapper;

import java.util.List;

import static sk.stuba.fei.api.herbiobe.controller.ControllerConstants.CANNOT_FIND_ROOM_WITH_ID;
import static sk.stuba.fei.api.herbiobe.controller.ControllerConstants.CANNOT_FIND_WATER_TANK_WITH_ID;

@CrossOrigin
@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class WaterTankRestController {

    private final WaterTankRepository waterTankRepository;
    private final RoomRepository roomRepository;
    private final WaterTankMapper waterTankMapper;

    @ApiOperation("Get all water tanks in the specified room")
    @GetMapping(value = "rooms/{roomId}/waterTanks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WaterTankEntity> findAllByRoom(@PathVariable Integer roomId) {
        if (!roomRepository.existsById(roomId)) {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId);
        } else {
            return waterTankRepository.findByRoomId(roomId);
        }
    }

    @ApiOperation("Get water tank by room and id")
    @GetMapping(value = "rooms/{roomId}/waterTanks/{waterTankId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WaterTankEntity findByRoom(@PathVariable Integer roomId, @PathVariable Integer waterTankId) {
        if (!roomRepository.existsById(roomId)) {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId);
        } else {
            return waterTankRepository.findByIdAndRoomId(waterTankId, roomId)
                .orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_WATER_TANK_WITH_ID + waterTankId));
        }
    }

    @ApiOperation("Create water tank in specified room")
    @PostMapping(
        value = "rooms/{roomId}/waterTanks",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public WaterTankEntity create(@PathVariable Integer roomId, @RequestBody CreateWaterTankRequest createWaterTankRequest) {
        return roomRepository.findById(roomId).map(room -> {
            var waterTank = waterTankMapper.createEntityFromRequest(createWaterTankRequest);
            room.addWaterTank(waterTank);
            return waterTankRepository.save(waterTank);
        }).orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId));
    }

    @ApiOperation("Update water tank in specified room")
    @PutMapping(
        value = "rooms/{roomId}/waterTanks/{waterTankId}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public WaterTankEntity update(
        @PathVariable Integer roomId,
        @PathVariable Integer waterTankId,
        @RequestBody UpdateWaterTankRequest updateWaterTankRequest
    ) {
        if (!roomRepository.existsById(roomId)) {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId);
        }

        return waterTankRepository.findById(waterTankId).map(waterTank -> {
            waterTankMapper.mapRequestToEntity(updateWaterTankRequest, waterTank);
            return waterTankRepository.save(waterTank);
        }).orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_WATER_TANK_WITH_ID + waterTankId));
    }

    @ApiOperation("Delete water tank from specified room")
    @DeleteMapping("rooms/{roomId}/waterTanks/{waterTankId}")
    public void delete(@PathVariable Integer roomId, @PathVariable Integer waterTankId) {
        if (!roomRepository.existsById(roomId)) {
            throw new ResourceNotFoundException(CANNOT_FIND_ROOM_WITH_ID + roomId);
        } else if (waterTankRepository.existsById(waterTankId)) {
            waterTankRepository.deleteById(waterTankId);
        } else {
            throw new ResourceNotFoundException(CANNOT_FIND_WATER_TANK_WITH_ID + waterTankId);
        }
    }
}
