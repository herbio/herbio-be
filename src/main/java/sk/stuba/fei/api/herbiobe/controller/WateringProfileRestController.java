package sk.stuba.fei.api.herbiobe.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;
import sk.stuba.fei.api.herbiobe.exception.ResourceAlreadyExistsException;
import sk.stuba.fei.api.herbiobe.exception.ResourceNotFoundException;
import sk.stuba.fei.api.herbiobe.model.entity.WateringProfileEntity;
import sk.stuba.fei.api.herbiobe.model.notifications.WateringModeChangedNotification;
import sk.stuba.fei.api.herbiobe.model.payload.request.CreateWateringProfileRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.UpdateWateringProfileRequest;
import sk.stuba.fei.api.herbiobe.repository.PlantRepository;
import sk.stuba.fei.api.herbiobe.repository.WateringProfileRepository;
import sk.stuba.fei.api.herbiobe.service.mapper.WateringProfileMapper;
import sk.stuba.fei.api.herbiobe.service.mqtt.MqttConstants;
import sk.stuba.fei.api.herbiobe.service.mqtt.MqttPushClient;

import java.util.List;

import static sk.stuba.fei.api.herbiobe.controller.ControllerConstants.*;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class WateringProfileRestController {

    private final PlantRepository plantRepository;
    private final WateringProfileRepository wateringProfileRepository;
    private final WateringProfileMapper wateringProfileMapper;
    private final MqttPushClient mqttPushClient;

    @ApiOperation("Get all watering profiles")
    @GetMapping(value = "wateringProfiles", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WateringProfileEntity> findAll() {
        return wateringProfileRepository.findAll();
    }

    @ApiOperation("Get watering profile by id")
    @GetMapping(value = "wateringProfiles/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WateringProfileEntity findById(@PathVariable Integer id) {
        return wateringProfileRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_WATERING_PROFILE_WITH_ID + id));
    }

    @ApiOperation("Get watering profile for plant by plant id")
    @GetMapping(value = "wateringProfiles/plant/{plantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WateringProfileEntity findByPlantId(@PathVariable Integer plantId) {
        return wateringProfileRepository.findByPlantId(plantId)
            .orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_PLANT_WITH_ID + plantId));
    }

    @ApiOperation("Create watering profile for specified plant")
    @PostMapping(
        value = "wateringProfiles/plant/{plantId}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public WateringProfileEntity create(@PathVariable Integer plantId, @RequestBody CreateWateringProfileRequest request) {
        return plantRepository.findById(plantId).map(plant -> {
            if (plant.getWateringProfile() != null) {
                throw new ResourceAlreadyExistsException(PLANT_ALREADY_HAS_WATERING_PROFILE);
            }

            var profile = wateringProfileMapper.createEntityFromRequest(request);
            plant.addWateringProfile(profile);

            return wateringProfileRepository.save(profile);
        }).orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_PLANT_WITH_ID + plantId));
    }

    @ApiOperation("Update watering profile")
    @PutMapping(
        value = "wateringProfiles/{id}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public WateringProfileEntity update(@PathVariable Integer id, @RequestBody UpdateWateringProfileRequest request) {
        return wateringProfileRepository.findById(id).map(profile -> {
            final var previousWateringMode = profile.getWateringMode();
            wateringProfileMapper.mapRequestToEntity(request, profile);

            // if watering mode has changed, we notify hardware
            if (previousWateringMode != profile.getWateringMode()) {
                notifyWateringModeChanged(
                    WateringModeChangedNotification.builder()
                        .plantId(profile.getPlant().getId())
                        .wateringMode(profile.getWateringMode())
                        .wateringAmount(profile.getWateringAmount())
                        .humidityThresholdDown(profile.getHumidityThresholdDown())
                        .humidityThresholdUp(profile.getHumidityThresholdUp())
                        .build()
                );
            }

            return wateringProfileRepository.save(profile);
        }).orElseThrow(() -> new ResourceNotFoundException(CANNOT_FIND_WATERING_PROFILE_WITH_ID + id));
    }

    @ApiOperation("Delete watering profile by id")
    @DeleteMapping("wateringProfiles/{id}")
    public void delete(@PathVariable Integer id) {
        var wateringProfileEntityOptional = wateringProfileRepository.findById(id);

        if (wateringProfileEntityOptional.isEmpty()) {
            throw new ResourceNotFoundException(CANNOT_FIND_WATERING_PROFILE_WITH_ID + id);
        }

        var wateringProfileEntity = wateringProfileEntityOptional.get();

        var plantOptional = plantRepository.findById(wateringProfileEntity.getPlant().getId());
        if (plantOptional.isEmpty()) {
            throw new ResourceNotFoundException(CANNOT_FIND_PLANT_WITH_ID + wateringProfileEntity.getPlant().getId());
        }

        wateringProfileRepository.deleteById(id);

        var plant = plantOptional.get();
        plant.setWateringProfile(null);
        plantRepository.save(plant);
    }

    @Async
    void notifyWateringModeChanged(final WateringModeChangedNotification wateringModeChangedNotification) {
        try {
            mqttPushClient.publish(MqttConstants.TOPIC_WATERING_MODE, wateringModeChangedNotification.writeAsString());
            log.info(INF_SENT_WATERING_MODE_NOTIF, wateringModeChangedNotification.getPlantId(), wateringModeChangedNotification.getWateringMode());
        } catch (JsonProcessingException e) {
            log.error(ERR_CANNOT_NOTIFY_DUE_JSON_PARSE_ERROR, MqttConstants.TOPIC_WATERING_MODE);
        }
    }
}
