package sk.stuba.fei.api.herbiobe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.Instant;
import java.util.stream.Collectors;

@ControllerAdvice
public class ControllerAdviceImpl {

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handle(MethodArgumentNotValidException exception) {
        return ErrorResponse
            .builder()
            .timestamp(Instant.now())
            .message("Field validation error(s)")
            .status(HttpStatus.BAD_REQUEST.value())
            .fieldErrors(exception
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .filter(fieldError -> fieldError.getDefaultMessage() != null)
                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage))
            )
            .build();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ResourceNotFoundException.class)
    public ErrorResponse handle(ResourceNotFoundException exception) {
        return ErrorResponse
            .builder()
            .timestamp(Instant.now())
            .message(exception.getMessage())
            .status(HttpStatus.BAD_REQUEST.value())
            .fieldErrors(null)
            .build();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public ErrorResponse handle(ResourceAlreadyExistsException exception) {
        return ErrorResponse
            .builder()
            .timestamp(Instant.now())
            .message(exception.getMessage())
            .status(HttpStatus.CONFLICT.value())
            .fieldErrors(null)
            .build();
    }
}
