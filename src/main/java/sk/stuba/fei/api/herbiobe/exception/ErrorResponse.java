package sk.stuba.fei.api.herbiobe.exception;

import lombok.*;

import java.time.Instant;
import java.util.Map;

@Data
@Builder
@With
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {

    private Instant timestamp;
    private String message;
    private Integer status;
    private Map<String, String> fieldErrors;
}
