package sk.stuba.fei.api.herbiobe.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import sk.stuba.fei.api.herbiobe.model.entity.UserEntity;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailsImpl implements UserDetails {

    private UUID id;
    private String username;
    private String email;
    private Boolean enabled = false;
    private String password;
    private Collection<GrantedAuthority> authorities;

    public static UserDetailsImpl fromEntity(final UserEntity userEntity) {
        return UserDetailsImpl
            .builder()
            .id(userEntity.getId())
            .username(userEntity.getUsername())
            .email(userEntity.getEmail())
            .enabled(userEntity.getEnabled())
            .password(userEntity.getPassword())
            .authorities(userEntity
                .getRoles()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toSet()))
            .build();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
