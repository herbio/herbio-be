package sk.stuba.fei.api.herbiobe.model.entity;

public enum ELightingRequirement {
    VERY_LOW, LOW, MEDIUM, HIGH, VERY_HIGH
}
