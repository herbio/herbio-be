package sk.stuba.fei.api.herbiobe.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ERole {
    ROLE_USER("USER"),
    ROLE_ADMIN("ADMIN");

    private final String value;
}
