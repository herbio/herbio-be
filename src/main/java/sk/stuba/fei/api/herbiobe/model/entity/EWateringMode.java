package sk.stuba.fei.api.herbiobe.model.entity;

public enum EWateringMode {
    AUTO, MANUAL
}
