package sk.stuba.fei.api.herbiobe.model.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.MappedSuperclass;
import java.time.Instant;

@Getter
@Setter
@MappedSuperclass
public abstract class EntityWithTimestamps {

    @CreationTimestamp
    protected Instant created;

    @UpdateTimestamp
    protected Instant updated;
}
