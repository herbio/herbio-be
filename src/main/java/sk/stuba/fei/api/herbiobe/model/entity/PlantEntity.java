package sk.stuba.fei.api.herbiobe.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.concurrent.ThreadLocalRandom;

@Getter
@Setter
@With
@Builder
@Entity
@Table(name = "plants")
@AllArgsConstructor
@NoArgsConstructor
public class PlantEntity extends EntityWithTimestamps {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Lob
    private String description;

    private Integer waterRequirement;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ELightingRequirement lightingRequirement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    @JsonIgnore
    private RoomEntity room;

    @OneToOne(mappedBy = "plant", cascade = CascadeType.ALL, orphanRemoval = true)
    private WateringProfileEntity wateringProfile;

    public PlantEntity addWateringProfile(final WateringProfileEntity wateringProfile) {
        this.wateringProfile = wateringProfile;
        wateringProfile.setPlant(this);
        return this;
    }

    public static PlantEntity dummyPlant() {
        return PlantEntity
            .builder()
            .name("Plant " + ThreadLocalRandom.current().nextInt(10, 1000))
            .description("Desc " + ThreadLocalRandom.current().nextInt(10, 1000))
            .waterRequirement(120)
            .lightingRequirement(ELightingRequirement.VERY_HIGH)
            .build();
    }
}
