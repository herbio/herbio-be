package sk.stuba.fei.api.herbiobe.model.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@With
@Builder
@Entity
@Table(name = "refresh_tokens")
@AllArgsConstructor
@NoArgsConstructor
public class RefreshTokenEntity extends EntityWithTimestamps {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;
}
