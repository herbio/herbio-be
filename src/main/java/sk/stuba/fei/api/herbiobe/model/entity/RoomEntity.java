package sk.stuba.fei.api.herbiobe.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

@Getter
@Setter
@With
@Builder
@Entity
@Table(name = "rooms")
@AllArgsConstructor
@NoArgsConstructor
public class RoomEntity extends EntityWithTimestamps {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Lob
    private String description;

    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<PlantEntity> plants = new HashSet<>();

    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<WaterTankEntity> waterTanks = new HashSet<>();

    public RoomEntity addPlant(final PlantEntity plant) {
        this.plants.add(plant);
        plant.setRoom(this);
        return this;
    }

    public void removePlant(final PlantEntity plant) {
        this.plants.remove(plant);
        plant.setRoom(null);
    }

    public RoomEntity addWaterTank(final WaterTankEntity waterTank) {
        this.waterTanks.add(waterTank);
        waterTank.setRoom(this);
        return this;
    }

    public void removeWaterTank(final WaterTankEntity waterTank) {
        this.waterTanks.remove(waterTank);
        waterTank.setRoom(null);
    }

    public static RoomEntity dummyRoom() {
        return RoomEntity
            .builder()
            .name("Room " + ThreadLocalRandom.current().nextInt(10, 1000))
            .description("Desc " + ThreadLocalRandom.current().nextInt(10, 1000))
            .plants(new HashSet<>())
            .waterTanks(new HashSet<>())
            .build();
    }
}
