package sk.stuba.fei.api.herbiobe.model.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@With
@Builder
@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity extends EntityWithTimestamps {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(unique = true)
    private String username;

    private String password;

    @Column(unique = true)
    private String email;

    private Boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_roles",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<RoleEntity> roles = new HashSet<>();

    public void addRole(final RoleEntity role) {
        this.roles.add(role);
    }

    public void removeRole(final RoleEntity role) {
        this.roles.remove(role);
    }

    public static UserEntity dummyUser() {
        return UserEntity
            .builder()
            .username("user")
            .email("user@herbio.com")
            .enabled(true)
            .build();
    }

    public static UserEntity dummyAdmin() {
        return UserEntity
            .builder()
            .username("admin")
            .email("admin@herbio.com")
            .enabled(true)
            .build();
    }
}
