package sk.stuba.fei.api.herbiobe.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.concurrent.ThreadLocalRandom;

@Getter
@Setter
@With
@Builder
@Entity
@Table(name = "water_tanks")
@AllArgsConstructor
@NoArgsConstructor
public class WaterTankEntity extends EntityWithTimestamps {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Double capacity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    @JsonIgnore
    private RoomEntity room;

    public static WaterTankEntity dummyWaterTank() {
        return WaterTankEntity
            .builder()
            .capacity(ThreadLocalRandom.current().nextDouble(0, 10000))
            .build();
    }
}
