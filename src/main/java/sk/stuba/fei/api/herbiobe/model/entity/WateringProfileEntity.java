package sk.stuba.fei.api.herbiobe.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.concurrent.ThreadLocalRandom;

@Getter
@Setter
@With
@Builder
@Entity
@Table(name = "watering_profiles")
@AllArgsConstructor
@NoArgsConstructor
public class WateringProfileEntity extends EntityWithTimestamps {

    @Id
    private Integer id;

    private Integer wateringAmount;

    private Double humidityThresholdUp;

    private Double humidityThresholdDown;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EWateringMode wateringMode = EWateringMode.MANUAL;

    @OneToOne
    @MapsId
    @JsonIgnore
    private PlantEntity plant;

    public static WateringProfileEntity dummyProfile() {
        return WateringProfileEntity
            .builder()
            .wateringMode(EWateringMode.MANUAL)
            .wateringAmount(ThreadLocalRandom.current().nextInt(10, 1000))
            .humidityThresholdUp(ThreadLocalRandom.current().nextDouble(0, 100))
            .humidityThresholdDown(ThreadLocalRandom.current().nextDouble(0, 100))
            .build();
    }
}
