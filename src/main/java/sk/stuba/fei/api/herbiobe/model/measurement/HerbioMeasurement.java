package sk.stuba.fei.api.herbiobe.model.measurement;

import lombok.Data;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.TimeColumn;

import java.time.Instant;

@Data
public abstract class HerbioMeasurement {

    @TimeColumn
    @Column(name = "time")
    protected Instant time = Instant.now();
}
