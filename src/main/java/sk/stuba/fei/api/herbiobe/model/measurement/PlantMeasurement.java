package sk.stuba.fei.api.herbiobe.model.measurement;

import lombok.*;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import sk.stuba.fei.api.herbiobe.service.influxdb.InfluxDBConstants;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Measurement(name = InfluxDBConstants.MEASUREMENT_PLANT)
public class PlantMeasurement extends HerbioMeasurement {

    @Column(name = "id")
    private Integer id;

    @Column(name = "humidity")
    private Double humidity;

    @Column(name = "temperature")
    private Double temperature;

    @Override
    public String toString() {
        return "Plant { " +
            "time = " + time +
            ", id = '" + id + '\'' +
            ", humidity = " + humidity +
            "%, temperature = " + temperature +
            "°C }";
    }
}
