package sk.stuba.fei.api.herbiobe.model.measurement;

import lombok.*;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import sk.stuba.fei.api.herbiobe.service.influxdb.InfluxDBConstants;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Measurement(name = InfluxDBConstants.MEASUREMENT_ROOM)
public class RoomMeasurement extends HerbioMeasurement {

    @Column(name = "id")
    private Integer id;

    @Column(name = "humidity")
    private Double humidity;

    @Column(name = "lighting")
    private Double lighting;

    @Column(name = "temperature")
    private Double temperature;

    @Override
    public String toString() {
        return "Room { " +
            "time = " + time +
            ", id = '" + id + '\'' +
            ", humidity = " + humidity +
            "%, lighting = " + lighting +
            " lumen, temperature = " + temperature +
            "°C }";
    }
}
