package sk.stuba.fei.api.herbiobe.model.measurement;

import lombok.*;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import sk.stuba.fei.api.herbiobe.service.influxdb.InfluxDBConstants;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Measurement(name = InfluxDBConstants.MEASUREMENT_WATER_TANK)
public class WaterTankMeasurement extends HerbioMeasurement {

    @Column(name = "id")
    private Integer id;

    @Column(name = "water_level")
    private Double waterLevel;

    @Override
    public String toString() {
        return "WaterTank { time = " + time + ", id = " + id + ", waterLevel = " + waterLevel + "% }";
    }
}
