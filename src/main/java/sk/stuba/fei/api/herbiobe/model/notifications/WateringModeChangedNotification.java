package sk.stuba.fei.api.herbiobe.model.notifications;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sk.stuba.fei.api.herbiobe.model.entity.EWateringMode;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WateringModeChangedNotification {

    private Integer plantId;
    private EWateringMode wateringMode;
    private Integer wateringAmount;
    private Double humidityThresholdUp;
    private Double humidityThresholdDown;

    public String writeAsString() throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(this);
    }
}
