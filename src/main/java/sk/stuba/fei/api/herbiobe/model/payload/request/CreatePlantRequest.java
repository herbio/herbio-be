package sk.stuba.fei.api.herbiobe.model.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sk.stuba.fei.api.herbiobe.model.entity.ELightingRequirement;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatePlantRequest {

    private String name;
    private String description;
    private Integer waterRequirement;
    private ELightingRequirement lightingRequirement;
}
