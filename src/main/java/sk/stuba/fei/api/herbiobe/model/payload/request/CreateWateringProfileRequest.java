package sk.stuba.fei.api.herbiobe.model.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sk.stuba.fei.api.herbiobe.model.entity.EWateringMode;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateWateringProfileRequest {

    private Integer wateringAmount;
    private Double humidityThresholdUp;
    private Double humidityThresholdDown;
    private EWateringMode wateringMode;
}
