package sk.stuba.fei.api.herbiobe.model.payload.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationResponse {

    private UUID id;
    private String username;
    private String email;
    private List<String> roles;
    private String jwtToken;
    private String type;
    private Instant expiresAt;
    private String refreshToken;
}
