package sk.stuba.fei.api.herbiobe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.stuba.fei.api.herbiobe.model.entity.PlantEntity;

import java.util.List;
import java.util.Optional;

public interface PlantRepository extends JpaRepository<PlantEntity, Integer> {

    List<PlantEntity> findByRoomId(Integer roomId);

    Optional<PlantEntity> findByIdAndRoomId(Integer plantId, Integer roomId);
}
