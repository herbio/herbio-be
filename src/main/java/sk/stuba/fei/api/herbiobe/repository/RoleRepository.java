package sk.stuba.fei.api.herbiobe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.stuba.fei.api.herbiobe.model.entity.ERole;
import sk.stuba.fei.api.herbiobe.model.entity.RoleEntity;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {

    Optional<RoleEntity> findByName(ERole name);
}
