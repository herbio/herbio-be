package sk.stuba.fei.api.herbiobe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.stuba.fei.api.herbiobe.model.entity.RoomEntity;

public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {
}
