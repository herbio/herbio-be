package sk.stuba.fei.api.herbiobe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.stuba.fei.api.herbiobe.model.entity.UserEntity;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    Optional<UserEntity> findByUsername(String username);

    Optional<UserEntity> findByEmail(String email);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}

