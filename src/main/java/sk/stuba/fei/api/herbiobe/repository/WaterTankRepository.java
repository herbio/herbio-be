package sk.stuba.fei.api.herbiobe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.stuba.fei.api.herbiobe.model.entity.WaterTankEntity;

import java.util.List;
import java.util.Optional;

public interface WaterTankRepository extends JpaRepository<WaterTankEntity, Integer> {

    List<WaterTankEntity> findByRoomId(Integer roomId);

    Optional<WaterTankEntity> findByIdAndRoomId(Integer waterTankId, Integer roomId);
}
