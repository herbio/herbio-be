package sk.stuba.fei.api.herbiobe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.stuba.fei.api.herbiobe.model.entity.WateringProfileEntity;

import java.util.Optional;

public interface WateringProfileRepository extends JpaRepository<WateringProfileEntity, Integer> {

    Optional<WateringProfileEntity> findByPlantId(final Integer plantId);
}
