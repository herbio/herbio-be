package sk.stuba.fei.api.herbiobe.service.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sk.stuba.fei.api.herbiobe.model.payload.request.LoginRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.RefreshTokenRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.SignupRequest;
import sk.stuba.fei.api.herbiobe.model.payload.response.AuthenticationResponse;
import sk.stuba.fei.api.herbiobe.model.payload.response.MessageResponse;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final LoginService loginService;
    private final SignupService signupService;
    private final RefreshTokenService refreshTokenService;

    public ResponseEntity<AuthenticationResponse> login(final LoginRequest loginRequest) {
        return loginService.login(loginRequest);
    }

    public ResponseEntity<MessageResponse> signUp(final SignupRequest signupRequest) {
        return signupService.signUp(signupRequest);
    }

    public ResponseEntity<AuthenticationResponse> refreshToken(final RefreshTokenRequest refreshTokenRequest) {
        return refreshTokenService.refreshToken(refreshTokenRequest);
    }

    public ResponseEntity<MessageResponse> logout(final RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.deleteRefreshToken(refreshTokenRequest.getRefreshToken());
        return ResponseEntity.ok(new MessageResponse("Refresh token deleted successfully"));
    }
}
