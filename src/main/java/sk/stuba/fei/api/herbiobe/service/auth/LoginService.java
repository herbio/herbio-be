package sk.stuba.fei.api.herbiobe.service.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import sk.stuba.fei.api.herbiobe.model.UserDetailsImpl;
import sk.stuba.fei.api.herbiobe.model.entity.RefreshTokenEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.LoginRequest;
import sk.stuba.fei.api.herbiobe.model.payload.response.AuthenticationResponse;
import sk.stuba.fei.api.herbiobe.repository.RefreshTokenRepository;
import sk.stuba.fei.api.herbiobe.service.security.JwtProvider;

import java.time.Instant;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;
    private final RefreshTokenRepository refreshTokenRepository;

    public ResponseEntity<AuthenticationResponse> login(final LoginRequest loginRequest) {
        var authentication = authenticationManager
            .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        var jwtToken = jwtProvider.generateJwtToken(authentication);
        var refreshToken = refreshTokenRepository.save(
            RefreshTokenEntity
                .builder()
                .id(null)
                .token(UUID.randomUUID().toString())
                .build()
        );

        var userDetails = (UserDetailsImpl) authentication.getPrincipal();
        var roles = userDetails
            .getAuthorities()
            .stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList());

        return ResponseEntity.ok(
            AuthenticationResponse
                .builder()
                .id(userDetails.getId())
                .jwtToken(jwtToken)
                .type("Bearer token")
                .username(userDetails.getUsername())
                .email(userDetails.getEmail())
                .roles(roles)
                .refreshToken(refreshToken.getToken())
                .expiresAt(Instant.now().plusMillis(jwtProvider.getJwtExpirationMs()))
                .build()
        );
    }
}
