package sk.stuba.fei.api.herbiobe.service.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sk.stuba.fei.api.herbiobe.exception.ResourceNotFoundException;
import sk.stuba.fei.api.herbiobe.model.payload.request.RefreshTokenRequest;
import sk.stuba.fei.api.herbiobe.model.payload.response.AuthenticationResponse;
import sk.stuba.fei.api.herbiobe.repository.RefreshTokenRepository;
import sk.stuba.fei.api.herbiobe.repository.UserRepository;
import sk.stuba.fei.api.herbiobe.service.security.JwtProvider;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RefreshTokenService {

    private final RefreshTokenRepository refreshTokenRepository;
    private final JwtProvider jwtProvider;
    private final UserRepository userRepository;

    public ResponseEntity<AuthenticationResponse> refreshToken(final RefreshTokenRequest refreshTokenRequest) {
        validateRefreshToken(refreshTokenRequest.getRefreshToken());
        var token = jwtProvider.generateJwtTokenWithUsername(refreshTokenRequest.getUsername());
        var user = userRepository.findByUsername(refreshTokenRequest.getUsername())
            .orElseThrow(() -> new ResourceNotFoundException("User not found with username " + refreshTokenRequest.getUsername()));

        return ResponseEntity.ok(
            AuthenticationResponse
                .builder()
                .jwtToken(token)
                .id(user.getId())
                .type(null)
                .username(user.getUsername())
                .email(user.getEmail())
                .roles(user.getRoles().stream().map(role -> role.getName().name()).collect(Collectors.toList()))
                .refreshToken(refreshTokenRequest.getRefreshToken())
                .expiresAt(Instant.now().plusMillis(jwtProvider.getJwtExpirationMs()))
                .build()
        );
    }

    @Transactional
    public void deleteRefreshToken(final String token) {
        refreshTokenRepository.deleteByToken(token);
    }

    private void validateRefreshToken(final String token) {
        refreshTokenRepository.findByToken(token).orElseThrow(() -> new ResourceNotFoundException("Invalid refresh token"));
    }
}
