package sk.stuba.fei.api.herbiobe.service.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sk.stuba.fei.api.herbiobe.exception.ResourceNotFoundException;
import sk.stuba.fei.api.herbiobe.model.entity.ERole;
import sk.stuba.fei.api.herbiobe.model.entity.UserEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.SignupRequest;
import sk.stuba.fei.api.herbiobe.model.payload.response.MessageResponse;
import sk.stuba.fei.api.herbiobe.repository.RoleRepository;
import sk.stuba.fei.api.herbiobe.repository.UserRepository;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class SignupService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public ResponseEntity<MessageResponse> signUp(final SignupRequest signupRequest) {
        if (userRepository.existsByUsername(signupRequest.getUsername()))
            return ResponseEntity.badRequest().body(new MessageResponse("Username is already taken"));

        if (userRepository.existsByEmail(signupRequest.getEmail()))
            return ResponseEntity.badRequest().body(new MessageResponse("Email is already in use"));

        var user = UserEntity
            .builder()
            .username(signupRequest.getUsername())
            .email(signupRequest.getEmail())
            .password(passwordEncoder.encode(signupRequest.getPassword()))
            .enabled(true)
            .roles(Set.of(roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new ResourceNotFoundException("Error: role not found"))))
            .build();

        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User signed up"));
    }
}
