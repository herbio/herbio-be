package sk.stuba.fei.api.herbiobe.service.influxdb;

import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InfluxDBConfiguration {

    @Bean
    public InfluxDBResultMapper getInfluxDBResultMapper() {
        return new InfluxDBResultMapper();
    }
}
