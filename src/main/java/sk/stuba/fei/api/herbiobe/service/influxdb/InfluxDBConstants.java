package sk.stuba.fei.api.herbiobe.service.influxdb;

public class InfluxDBConstants {

    private InfluxDBConstants() {
    }

    // version
    public static final String UNKNOWN_VERSION = "unknown";

    // info messages
    public static final String INF_CONNECTED = "Connected to InfluxDB server {}, as {}:{}";
    public static final String INF_BATCH_ENABLED = "Enabled batching for DB: '{}'. Size: {}, interval {} ms";
    public static final String INF_BATCH_DISABLED = "Disabled batching";

    // warning messages
    public static final String WRN_NOT_CONNECTED = "Not connected to InfluxDB server";

    // error messages
    public static final String ERR_PING_SERVER = "Error pinging InfluxDB server";
    public static final String ERR_QUERY_RESULT = "Error occurred in query: {}";

    // measurements
    public static final String MEASUREMENT_PLANT = "plant_measurement";
    public static final String MEASUREMENT_ROOM = "room_measurement";
    public static final String MEASUREMENT_WATER_TANK = "water_tank_measurement";

    // queries
    public static final String QUERY_SELECT_ALL_PLANT_INFO = "select * from " + MEASUREMENT_PLANT;
    public static final String QUERY_SELECT_ALL_ROOM_INFO = "select * from " + MEASUREMENT_ROOM;
    public static final String QUERY_SELECT_ALL_WATER_TANK_INFO = "select * from " + MEASUREMENT_WATER_TANK;
}
