package sk.stuba.fei.api.herbiobe.service.influxdb;

import lombok.extern.slf4j.Slf4j;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.stuba.fei.api.herbiobe.model.measurement.PlantMeasurement;
import sk.stuba.fei.api.herbiobe.model.measurement.RoomMeasurement;
import sk.stuba.fei.api.herbiobe.model.measurement.WaterTankMeasurement;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static sk.stuba.fei.api.herbiobe.service.influxdb.InfluxDBConstants.MEASUREMENT_PLANT;

@Slf4j
@Service
public class InfluxDBHandler {

    private InfluxDB influxDB;
    private boolean isConnected = false;
    private final InfluxDBProperties influxDBProperties;
    private final InfluxDBResultMapper influxDBResultMapper;

    @Autowired
    public InfluxDBHandler(InfluxDBProperties influxDBProperties, InfluxDBResultMapper influxDBResultMapper) {
        this.influxDBProperties = influxDBProperties;
        this.influxDBResultMapper = influxDBResultMapper;

        // connect
        connect(influxDBProperties.getUrl(), influxDBProperties.getUsername(), influxDBProperties.getPassword());

        // create retention policies todo
        createRetentionPolicy("two_hours", influxDBProperties.getDatabase(), "2h", true);
        createRetentionPolicy("a_year", influxDBProperties.getDatabase(), "52w", false);
        createContinuousQuery(
            "cq_30m",
            influxDBProperties.getDatabase(),
            Set.of("humidity", "temperature"),
            "a_year",
            "downsampled_" + MEASUREMENT_PLANT,
            MEASUREMENT_PLANT,
            "30m"
        );

        // enable batching
        enableBatch(
            50,                                         // batch size
            5000,                                    // batch save interval (i. e. save batch after x units)
            TimeUnit.MILLISECONDS,                          // batch save interval time unit
            influxDBProperties.getRetentionPolicy(),
            influxDBProperties.getDatabase());
    }

    public void connect(final String url, final String username, final String password) {
        influxDB = InfluxDBFactory.connect(url, username, password);

        var response = influxDB.ping();

        if (!response.isGood() || response.getVersion().equalsIgnoreCase(InfluxDBConstants.UNKNOWN_VERSION)) {
            isConnected = false;
            throw new RuntimeException(InfluxDBConstants.ERR_PING_SERVER);
        } else {
            isConnected = true;
            log.info(
                InfluxDBConstants.INF_CONNECTED,
                influxDBProperties.getUrl(),
                influxDBProperties.getUsername(),
                influxDBProperties.getPassword()
            );
        }
    }

    public void enableBatch(int size, int interval, TimeUnit timeUnit, String retentionPolicy, String database) {
        if (isConnected()) {
            influxDB.enableBatch(size, interval, timeUnit);
            influxDB.setRetentionPolicy(retentionPolicy);
            influxDB.setDatabase(database);
            log.info(InfluxDBConstants.INF_BATCH_ENABLED, database, size, interval);
        } else {
            log.warn(InfluxDBConstants.WRN_NOT_CONNECTED);
        }
    }

    public void disableBatch() {
        if (isConnected()) {
            influxDB.disableBatch();
            log.info(InfluxDBConstants.INF_BATCH_DISABLED);
        } else {
            log.warn(InfluxDBConstants.WRN_NOT_CONNECTED);
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    public <T> void save(final T item) {
        if (isConnected()) {
            log.info("Saving: {}", item);

            influxDB.write(Point
                .measurementByPOJO(item.getClass())
                .addFieldsFromPOJO(item)
                .build());
        } else {
            log.warn(InfluxDBConstants.WRN_NOT_CONNECTED);
        }
    }

    public List<PlantMeasurement> getPlantMeasurements() {
        return getMeasurementsForClass(InfluxDBConstants.QUERY_SELECT_ALL_PLANT_INFO, PlantMeasurement.class);
    }

    public List<RoomMeasurement> getRoomMeasurements() {
        return getMeasurementsForClass(InfluxDBConstants.QUERY_SELECT_ALL_ROOM_INFO, RoomMeasurement.class);
    }

    public List<WaterTankMeasurement> getWaterTankMeasurements() {
        return getMeasurementsForClass(InfluxDBConstants.QUERY_SELECT_ALL_WATER_TANK_INFO, WaterTankMeasurement.class);
    }

    private <T> List<T> getMeasurementsForClass(final String selectQuery, final Class<T> measurementClass) {
        if (isConnected()) {
            var queryResult = runQuery(new Query(selectQuery, influxDBProperties.getDatabase()));

            if (queryResult.hasError()) {
                log.error(InfluxDBConstants.ERR_QUERY_RESULT, queryResult.getError());
                return Collections.emptyList();
            }

            return influxDBResultMapper.toPOJO(queryResult, measurementClass);
        } else {
            log.warn(InfluxDBConstants.WRN_NOT_CONNECTED);
            return Collections.emptyList();
        }
    }

    public QueryResult runQuery(final Query query) {
        return influxDB.query(query);
    }

    private void createRetentionPolicy(final String name, final String databaseName, final String duration, final boolean setDefault) {
        if (isConnected()) {

            final var stringBuilder = new StringBuilder("CREATE RETENTION POLICY \"");
            stringBuilder.append(name)
                .append("\" ON \"")
                .append(databaseName)
                .append("\" DURATION ")
                .append(duration)
                .append(" REPLICATION 1");

            if (setDefault) {
                stringBuilder.append(" DEFAULT");
            }

            var queryResult = runQuery(new Query(stringBuilder.toString(), influxDBProperties.getDatabase()));

            if (queryResult.hasError()) {
                log.error(InfluxDBConstants.ERR_QUERY_RESULT, queryResult.getError());
            }
        } else {
            log.warn(InfluxDBConstants.WRN_NOT_CONNECTED);
        }
    }

    private void createContinuousQuery(
        final String name,
        final String databaseName,
        final Set<String> fields,
        final String retentionPolicyInto,
        final String measurementInto,
        final String measurementFrom,
        final String groupByTime
    ) {
        if (isConnected()) {

            final var stringBuilder = new StringBuilder("CREATE CONTINUOUS QUERY \"");
            stringBuilder
                .append(name)
                .append("\" ON \"")
                .append(databaseName)
                .append("\" BEGIN SELECT ");
            fields.forEach(field -> stringBuilder
                .append("mean(\"")
                .append(field)
                .append("\") AS \"mean_")
                .append(field)
                .append("\","));
            stringBuilder
                .deleteCharAt(stringBuilder.length() - 1) // delete last comma
                .append(" INTO \"" + retentionPolicyInto + "\".\"" + measurementInto + "\" ")
                .append("FROM \"" + measurementFrom + "\" ")
                .append("GROUP BY time(" + groupByTime + ") END");

            System.out.println("CQ: " + stringBuilder.toString());

            var queryResult = runQuery(new Query(stringBuilder.toString(), influxDBProperties.getDatabase()));

            if (queryResult.hasError()) {
                log.error(InfluxDBConstants.ERR_QUERY_RESULT, queryResult.getError());
            }
        } else {
            log.warn(InfluxDBConstants.WRN_NOT_CONNECTED);
        }
    }
}
