package sk.stuba.fei.api.herbiobe.service.influxdb;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class InfluxDBProperties {

    private final String url;
    private final String username;
    private final String password;
    private final String database;
    private final String retentionPolicy;

    public InfluxDBProperties(
        @Value("${influxdb.url}") String url,
        @Value("${influxdb.username}") String username,
        @Value("${influxdb.password}") String password,
        @Value("${influxdb.database}") String database,
        @Value("${influxdb.retention-policy}") String retentionPolicy
    ) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.database = database;
        this.retentionPolicy = retentionPolicy;
    }
}
