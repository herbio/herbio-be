package sk.stuba.fei.api.herbiobe.service.live;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import sk.stuba.fei.api.herbiobe.model.measurement.HerbioMeasurement;

@Service
@RequiredArgsConstructor
public class LiveService {

    private final ApplicationEventPublisher publisher;

    public void publishMeasurementInfo(final HerbioMeasurement measurement) {
        publisher.publishEvent(measurement);
    }
}
