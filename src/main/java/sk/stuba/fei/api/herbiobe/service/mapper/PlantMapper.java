package sk.stuba.fei.api.herbiobe.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import sk.stuba.fei.api.herbiobe.model.entity.PlantEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.CreatePlantRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.UpdatePlantRequest;

@Mapper(config = MapperConfig.class)
public interface PlantMapper {

    PlantEntity createEntityFromRequest(final CreatePlantRequest request);

    void mapRequestToEntity(final UpdatePlantRequest request, @MappingTarget final PlantEntity entity);
}
