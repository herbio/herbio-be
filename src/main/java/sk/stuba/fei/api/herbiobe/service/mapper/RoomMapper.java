package sk.stuba.fei.api.herbiobe.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import sk.stuba.fei.api.herbiobe.model.entity.RoomEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.CreateRoomRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.UpdateRoomRequest;

@Mapper(config = MapperConfig.class)
public interface RoomMapper {

    RoomEntity createEntityFromRequest(final CreateRoomRequest request);

    void mapRequestToEntity(final UpdateRoomRequest request, @MappingTarget final RoomEntity entity);
}
