package sk.stuba.fei.api.herbiobe.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import sk.stuba.fei.api.herbiobe.model.entity.WaterTankEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.CreateWaterTankRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.UpdateWaterTankRequest;

@Mapper(config = MapperConfig.class)
public interface WaterTankMapper {

    WaterTankEntity createEntityFromRequest(final CreateWaterTankRequest request);

    void mapRequestToEntity(final UpdateWaterTankRequest request, @MappingTarget final WaterTankEntity entity);
}
