package sk.stuba.fei.api.herbiobe.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import sk.stuba.fei.api.herbiobe.model.entity.WateringProfileEntity;
import sk.stuba.fei.api.herbiobe.model.payload.request.CreateWateringProfileRequest;
import sk.stuba.fei.api.herbiobe.model.payload.request.UpdateWateringProfileRequest;

@Mapper(config = MapperConfig.class)
public interface WateringProfileMapper {

    WateringProfileEntity createEntityFromRequest(final CreateWateringProfileRequest request);

    void mapRequestToEntity(final UpdateWateringProfileRequest request, @MappingTarget final WateringProfileEntity entity);
}
