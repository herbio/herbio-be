package sk.stuba.fei.api.herbiobe.service.mqtt;

public class MqttConstants {

    private MqttConstants() {
    }

    // topics
    public static final String TOPIC_PLANT_MEASUREMENT = "topic/plant_info";
    public static final String TOPIC_ROOM_MEASUREMENT = "topic/room_info";
    public static final String TOPIC_WATER_TANK_MEASUREMENT = "topic/water_tank_info";
    public static final String TOPIC_WATERING_MODE = "topic/watering_mode";

    // info messages
    public static final String INF_CONNECTED = "Connected to MQTT broker {}. ClientID: {}. Username: {}";
    public static final String INF_SUBSCRIBED = "Subscribed to topic {}";

    // warning messages
    public static final String WRG_NOT_CONNECTED = "Client is not connected to the broker";
    public static final String WRG_ALREADY_CONNECTED = "Client is already connected to the broker";
    public static final String WRG_DISCONNECTED = "Client disconnected: {}.\nTrying to reconnect...";
    public static final String WRG_UNKNOWN_MSG_ARRIVED = "Unknown message arrived from topic {}";
    public static final String WRG_COULD_NOT_PROCESS_PAYLOAD = "Couldn't process MQTT message payload: '{}'";

    // error messages
    public static final String ERR_PUBLISHING = "Error publishing message";
    public static final String ERR_SUBSCRIBING = "Error subscribing to topic {}";
    public static final String ERR_WHILE_CONNECTING = "Error while connecting to broker: {}";
    public static final String ERR_CREATING_CLIENT = "Error while creating client";
}