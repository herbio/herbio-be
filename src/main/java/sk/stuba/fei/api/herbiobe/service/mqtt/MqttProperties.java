package sk.stuba.fei.api.herbiobe.service.mqtt;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class MqttProperties {

    private final String broker;
    private final String clientId;

    public MqttProperties(
        @Value("${mqtt.broker}") String broker,
        @Value("${mqtt.client-id}") String clientId
    ) {
        this.broker = broker;
        this.clientId = clientId;
    }
}
