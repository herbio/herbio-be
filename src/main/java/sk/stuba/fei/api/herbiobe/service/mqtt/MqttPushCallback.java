package sk.stuba.fei.api.herbiobe.service.mqtt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Service;
import sk.stuba.fei.api.herbiobe.model.measurement.HerbioMeasurement;
import sk.stuba.fei.api.herbiobe.model.measurement.PlantMeasurement;
import sk.stuba.fei.api.herbiobe.model.measurement.RoomMeasurement;
import sk.stuba.fei.api.herbiobe.model.measurement.WaterTankMeasurement;
import sk.stuba.fei.api.herbiobe.service.influxdb.InfluxDBHandler;
import sk.stuba.fei.api.herbiobe.service.live.LiveService;

@Slf4j
@Service
@RequiredArgsConstructor
public class MqttPushCallback implements MqttCallback {

    private final ObjectMapper objectMapper;
    private final MqttPushClient mqttPushClient;
    private final MqttConnectOptions mqttConnectOptions;
    private final InfluxDBHandler influxDBHandler;
    private final LiveService liveService;

    @Override
    public void connectionLost(final Throwable throwable) {
        // after the connection is lost, it is usually reconnected here
        log.warn(MqttConstants.WRG_DISCONNECTED, throwable.getMessage());
        mqttPushClient.connect(mqttConnectOptions);
    }

    @Override
    public void messageArrived(final String topic, final MqttMessage mqttMessage) {
        var payload = new String(mqttMessage.getPayload());
        HerbioMeasurement measurement;

        try {
            switch (topic) {
                case MqttConstants.TOPIC_PLANT_MEASUREMENT:
                    measurement = objectMapper.readValue(payload, PlantMeasurement.class);
                    break;
                case MqttConstants.TOPIC_ROOM_MEASUREMENT:
                    measurement = objectMapper.readValue(payload, RoomMeasurement.class);
                    break;
                case MqttConstants.TOPIC_WATER_TANK_MEASUREMENT:
                    measurement = objectMapper.readValue(payload, WaterTankMeasurement.class);
                    break;
                default:
                    log.warn(MqttConstants.WRG_UNKNOWN_MSG_ARRIVED, topic);
                    return;
            }

            influxDBHandler.save(measurement);
            liveService.publishMeasurementInfo(measurement);
        } catch (JsonProcessingException ignored) {
            log.warn(MqttConstants.WRG_COULD_NOT_PROCESS_PAYLOAD, payload);
        }
    }

    @Override
    public void deliveryComplete(final IMqttDeliveryToken iMqttDeliveryToken) {
        log.info("deliveryComplete --------- {}", iMqttDeliveryToken.isComplete());
    }
}
