package sk.stuba.fei.api.herbiobe.service.mqtt;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MqttPushClient {

    private MqttAsyncClient mqttAsyncClient;
    private final MqttProperties mqttProperties;

    @Autowired
    public MqttPushClient(
        @Lazy MqttPushCallback mqttPushCallback,
        MqttProperties mqttProperties,
        MqttConnectOptions mqttConnectOptions
    ) {
        this.mqttProperties = mqttProperties;

        try {
            mqttAsyncClient = new MqttAsyncClient(mqttProperties.getBroker(), mqttProperties.getClientId(), new MemoryPersistence());
            mqttAsyncClient.setCallback(mqttPushCallback);
        } catch (MqttException e) {
            log.error(MqttConstants.ERR_CREATING_CLIENT);
            e.printStackTrace();
        }

        connect(mqttConnectOptions);

        subscribe(MqttConstants.TOPIC_PLANT_MEASUREMENT);
        subscribe(MqttConstants.TOPIC_ROOM_MEASUREMENT);
        subscribe(MqttConstants.TOPIC_WATER_TANK_MEASUREMENT);
    }

    public void connect(final MqttConnectOptions mqttConnectOptions) {
        if (isConnected()) {
            log.warn(MqttConstants.WRG_ALREADY_CONNECTED);
        } else {
            try {
                mqttAsyncClient.connect(mqttConnectOptions).waitForCompletion();
                log.info(
                    MqttConstants.INF_CONNECTED,
                    mqttProperties.getBroker(),
                    mqttProperties.getClientId(),
                    mqttConnectOptions.getUserName()
                );
            } catch (Exception e) {
                log.error(MqttConstants.ERR_WHILE_CONNECTING, e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public boolean isConnected() {
        return mqttAsyncClient.isConnected();
    }

    public void subscribe(final String topic) {
        if (isConnected()) {
            try {
                mqttAsyncClient.subscribe(topic, 0);
                log.info(MqttConstants.INF_SUBSCRIBED, topic);
            } catch (MqttException e) {
                log.error(MqttConstants.ERR_SUBSCRIBING, topic);
            }
        } else {
            log.warn(MqttConstants.WRG_NOT_CONNECTED);
        }
    }

    public void publish(final String topic, final String message) {
        if (isConnected()) {
            try {
                mqttAsyncClient.publish(topic, new MqttMessage(message.getBytes())).waitForCompletion();
            } catch (MqttException e) {
                log.error(MqttConstants.ERR_PUBLISHING);
            }
        } else {
            log.warn(MqttConstants.WRG_NOT_CONNECTED);
        }

    }
}
