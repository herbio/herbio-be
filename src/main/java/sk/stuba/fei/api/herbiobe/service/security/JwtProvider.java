package sk.stuba.fei.api.herbiobe.service.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import sk.stuba.fei.api.herbiobe.model.UserDetailsImpl;

import java.time.Instant;
import java.util.Date;

@Service
public class JwtProvider {

    private final String jwtSecret;

    @Getter
    private final Long jwtExpirationMs;

    public JwtProvider(
        @Value("${jwt.secret}") String jwtSecret,
        @Value("${jwt.expiration_ms}") Long jwtExpirationMs
    ) {
        this.jwtSecret = jwtSecret;
        this.jwtExpirationMs = jwtExpirationMs;
    }

    public String generateJwtToken(final Authentication authentication) {
        return Jwts
            .builder()
            .setSubject(((UserDetailsImpl) authentication.getPrincipal()).getUsername())
            .setIssuedAt(Date.from(Instant.now()))
            .setExpiration(Date.from(Instant.now().plusMillis(jwtExpirationMs)))
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact();
    }

    public String generateJwtTokenWithUsername(final String username) {
        return Jwts
            .builder()
            .setSubject(username)
            .setIssuedAt(Date.from(Instant.now()))
            .setExpiration(Date.from(Instant.now().plusMillis(jwtExpirationMs)))
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact();
    }

    public String getUserNameFromJwtToken(final String token) {
        return Jwts
            .parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .getBody()
            .getSubject();
    }

    public Boolean validateJwtToken(final String token) {
        try {
            Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
